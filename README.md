#ETS Radio Manager

##An application to conveniently manage SCS Software's Euro Truck Simulator 2 in-game radios. 

![ETS Radio Manager screenshot](http://bitbucket.org/santigl/ets-radio-manager/downloads/screenshot.png)

##Instructions
Download [ETSRadioManager-v0.1-2.zip](https://bitbucket.org/santigl/ets-radio-manager/downloads/ETSRadioManager-v0.1-2.zip)
and extract it in a folder of your choosing. 

Then launch `ETSRadioManager.exe`. From there open your `live_streams.sii` file, which is most likely located at `My Documents\Euro Truck Simulator 2\live_streams.sii`. Edit your stations and save the file back to the same location, so that the game can read the changes.
##Changelog
2015-07-15: Added a new zip file that contains an additional dll. That missing dll prevented it from running in some releases of Windows 7 and Windows 8 and 10.

##Comments/suggestions
For any problems, comments or suggestions you'd like to make, create a new [issue](https://bitbucket.org/santigl/ets-radio-manager/issues?status=new&status=open).

##Technical Information
ETS Radio Manager was developed in C++ using the [Qt framework](http://qt-project.org/).